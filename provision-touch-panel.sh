#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install -y ntp
sudo timedatectl set-timezone Australia/Brisbane

sudo apt-get install -y git


# setup the display dirvers
cd ~/
git clone https://github.com/goodtft/LCD-show.git
cd LCD-show
sudo ./LCD35-show


# setup the kiosk mode
sudo apt-get install -y chromium-browser ttf-mscorefonts-installer unclutter x11-xserver-utils

cd ~/
touch .config/lxsession/LXDE-pi/autostart
echo "# Normal website that does not need any exceptions" >> .config/lxsession/LXDE-pi/autostart
echo "@/usr/bin/chromium-browser --incognito --start-maximized --kiosk http://homeauto-server:18234" >> .config/lxsession/LXDE-pi/autostart
echo "# Enable mixed http/https content, remember if invalid certs were allowed (ie self signed certs)" >> .config/lxsession/LXDE-pi/autostart
echo "#@/usr/bin/chromium-browser --incognito --start-maximized --kiosk --allow-running-insecure-content --remember-cert-error-decisions http://homeauto-server:18234" >> .config/lxsession/LXDE-pi/autostart
echo "@unclutter" >> .config/lxsession/LXDE-pi/autostart
echo "@xset s off" >> .config/lxsession/LXDE-pi/autostart
echo "@xset s noblank" >> .config/lxsession/LXDE-pi/autostart
echo "@xset -dpms" >> .config/lxsession/LXDE-pi/autostart
