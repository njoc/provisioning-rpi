#!/bin/bash

# base system setup
sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install -y ntp


# setup base packages
sudo apt-get install -y software-properties-common
sudo add-apt-repository universe
sudo systemctl disable ModemManager
sudo apt-get remove -y modemmanager
sudo apt-get install -y apparmor-utils apt-transport-https avahi-daemon ca-certificates curl dbus jq network-manager socat


# setup docker to host server components
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

sudo usermod -aG docker pi


# setup hassio
curl -sL "https://raw.githubusercontent.com/home-assistant/hassio-installer/master/hassio_install.sh" -o hassio_install.sh
sudo bash -s -- -m raspberrypi3 hassio_install.sh


# setup home-panel
sudo docker pull timmo001/home-panel
sudo docker run -d -p 8234:8234 --restart always timmo001/home-panel
